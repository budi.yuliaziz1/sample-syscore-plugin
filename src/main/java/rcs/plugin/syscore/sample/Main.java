/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rcs.plugin.syscore.sample;

/**
 *
 * @author Gradi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Package tPackage = Main.class.getPackage();
        String tVersion = tPackage.getImplementationVersion();
        String tName = tPackage.getName();
        if (tVersion != null) {
            System.out.println("Package: " + tName + "-" + tVersion);
        } else {
            System.out.println("Package: " + tName);
        }
    }

}
