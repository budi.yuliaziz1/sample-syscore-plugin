/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rcs.plugin.syscore.sample;

import id.co.vsi.common.JSON.JSONMessage;
import id.co.vsi.common.log.LogType;
import id.co.vsi.common.log.SystemLog;
import id.co.vsi.common.settings.SystemConfig;
import id.co.vsi.systemcore.jasoncore.JSONPlugin;
import id.co.vsi.systemcore.jasoncore.JSONPluginHandler;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author $Author:: gradi $: Author of last revision
 * @version $Revision:: 1 $: Last revision number
 * @since $LastChangedDate:: 2024-02-06 08:00:00 +0700 (Tue, 6 Feb 2024) $: Date
 * of last revision
 */
public class SampleJSONPlugin extends JSONPlugin {

    public static final String cRevisionNumber = "$Revision: 1 $";
    public static final SampleJSONPlugin cLoggingStaticHookObject = new SampleJSONPlugin();

    // runtime config
    public static final String cNHit = "nHit";
    public static final String cNStatusSuccesful = "nSuccessfulHit";
    public static final String cNStatusNegative = "nNegativeHit";
    public static final String cNStatusAbnormal = "nAbnormalHit";

    public SampleJSONPlugin() {
        mHandlerHashMap.put(HelloJSONHandler.class.getName(), new HelloJSONHandler(this));
    }

    @Override
    public String getModuleDescription() {
        return "Sample JSON Plugin";
    }

    public static String getPluginLogFolder() {
        return "sample";
    }

    @Override
    public Object clone() {
        SampleJSONPlugin t_Plugin = new SampleJSONPlugin();
        t_Plugin.setThreadID(this.getThreadID());
        return t_Plugin;
    }

    @Override
    public boolean canHandleMessage(JSONMessage pDownlineReq) {
        ArrayList<Object> tValidProducts = SystemConfig.getNameSpace(Constant.cModuleNamespace)
                .getArrayParameter(Constant.cConfValidProducts, Constant.cDefaultPAN);
        return tValidProducts.contains(pDownlineReq.getString(Constant.cFieldPAN));
    }

    @Override
    public Object execute(Object p_Object) {
        SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(cNHit);

        if (p_Object == null) {
            SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(cNStatusAbnormal);
            return null;
        }

        JSONMessage tDownlineReq = (JSONMessage) p_Object;

        for (JSONPluginHandler tHandler : mHandlerHashMap.values()) {
            if (tHandler.canHandleMessage(tDownlineReq)) {
                String tParameterKey = tDownlineReq.getString(Constant.cFieldMTI);
                SystemLog.getSingleton().log(this, LogType.STREAM, "DOWNLINE " + tDownlineReq.getString(Constant.cFieldMTI) + " " + tHandler.getNameHandler() + " : " + tDownlineReq.getMessageStream());
                SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(tParameterKey + SystemConfig.getSeparatorString() + cNHit);

                tHandler.setRequestingAddress(getRequestAddress());
                JSONMessage tResponseMessage = tHandler.handleMessage(tDownlineReq);

                SystemLog.getSingleton().log(this, LogType.STREAM, "DOWNLINE " + tResponseMessage.getString(Constant.cFieldMTI) + " " + tHandler.getNameHandler() + " : " + tResponseMessage.getMessageStream());

                String tMessageResponseCode = String.valueOf(tResponseMessage.getResponseCode());
                SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(tMessageResponseCode);
                SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(tParameterKey + SystemConfig.getSeparatorString() + tMessageResponseCode);

                return tResponseMessage;
            }
        }

        SystemConfig.getNameSpace(Constant.cModuleNamespace).incrementParameter(cNStatusAbnormal);
        SystemLog.getSingleton().log(this, LogType.ERROR, "No handler for message : " + p_Object + ", returning original request message.");

        SystemLog.getSingleton().log(this, LogType.STREAM, "DOWNLINE " + tDownlineReq.getString(Constant.cFieldMTI) + " : " + tDownlineReq.getMessageStream());

        return tDownlineReq;
    }

    @Override
    public void initialize() {
    }

    @Override
    public HashMap<String, HashMap<String, String>> performSelfTest() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getPluginId() {
        return Constant.cModuleNamespace;
    }
}
